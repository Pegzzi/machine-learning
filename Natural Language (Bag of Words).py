# -*- coding: utf-8 -*-
"""
Created on Sat May 27 21:10:31 2017

@author: luizp
"""

import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

# usaremos um tsv (tab separated) pq o csv (coma separated) usa virgula, e teremos virgulas nos textos
dataset = pd.read_csv('Restaurant_reviews.tsv', delimiter = '\t', quoting = 3) #fala que esta esportando um arquivo com delimiter de tab e ignora as aspas (quoting)

#cleaning the text
import re
import nltk
#no natural language processing, na matriz de correlação cada palavra tem uma coluna, portanto + palavras = + processamento
nltk.download('stopwords') #faz download de uma lista de palavras que são irrelevantes para a analise
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer
corpus=[]
for i in range(0,len(dataset)):
    review = re.sub('[^a-zA-Z]',' ', dataset['Review'][i])#retirar tudo que nao seja letras de a a z maiusculas e minusculas e substitui por espaços
    review = review.lower()#colocar tudo em lower case
    review= review.split()
    #review=[word for word in review if not word in stopwords.words('english')] #itera palavra por palavra e mantem somente as que nao estao na stopliste m ingles
    review=[word for word in review if not word in set(stopwords.words('english'))] #em objetos fica mais rapido do que varrer listas, bom para grandes textos 


    #Steming , taking the roots of the lext (ex: Loved, loving, love, -> lov)
    ps = PorterStemmer()
    review=[ps.stem(word) for word in review]

    #faz o dataframe virar uma string ja com as palavras tratadas
    review=' '.join(review)
    corpus.append(review)
    
    
#create the bag of words model
#vai criar uma matriz com cada review feito nas linhas e uma coluna para cada palavra unica encontrada nesses reviews, mostrando qtas vezes essa palavra aparece em cada review (cada linha)
from sklearn.feature_extraction.text import CountVectorizer
#cv= CountVectorizer()
cv= CountVectorizer(mex_feature=1500) #só usa as 1500 palavras mais frequentes
X= cv.fit_transform(corpus).toarray() #faz o fit nos dados e faz matriz de parsing

    